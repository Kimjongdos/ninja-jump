﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pendulum : MonoBehaviour
{
    private Rigidbody2D rb2d;
    public float leftPush;
    public float rightPush;
    public float velocity;

    void Start(){
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.angularVelocity = velocity;
    }

    void Update(){
        Push();
    }

    public void Push(){
        if(transform.rotation.z > 0 && transform.rotation.z < rightPush && rb2d.angularVelocity > 0 && rb2d.angularVelocity < velocity){
            rb2d.angularVelocity = velocity;
        }
        else if(transform.rotation.z < 0 && transform.rotation.z > leftPush && rb2d.angularVelocity < 0 && rb2d.angularVelocity > velocity * -1){
            rb2d.angularVelocity = velocity * -1;
        }
    }
}
