﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cofre : MonoBehaviour
{
    public GameObject KunaiCofre;
    public Animator anim;
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Espada"){
            anim.SetTrigger("Hit");
            Instantiate(KunaiCofre,transform.position,Quaternion.Euler(0,0,90),null);
        }
    }
}
