﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private float SpeedMax;
    private Rigidbody2D rb;
    public float FuerzaDeSalto;
    private bool jump;
    private bool grounded;
    private bool attack;
    private bool jump_throw;
    //VIDAS
    public int lifes = 5;
    public GameObject[] uiLifes;

    //Variables para el movimiento
    [SerializeField] float velocitymax;
    [SerializeField] int movementSpeed;
    public float h;
    private Animator anim;
    private Collider2D coll;
    //Variable para el movimiento del BG
    public BackgroundManager bg;
    public AudioSource Swort;
    public AudioSource KunaiSound;
    //KUNAI
    public KunaiManager kunai;
    public int nKunais = 0;
    public float timeToThrow;
    private float currentTime;
  
    public Text KunaiText;
    //Color
    Color m_NewColor;
    SpriteRenderer m_SpriteRenderer;
    //Llave y diamante
    public bool haveKey = false;
    public GameObject uiKey;


    void Awake(){
        //LLamo al componente rigidbody
        rb = GetComponent<Rigidbody2D>();
        gameObject.AddComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();
        coll  = GetComponentInChildren<Collider2D>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
       
        uiKey.SetActive(false);
         
        coll.enabled=false;
    }
    void Start(){
        Cursor.visible = false;
    }
    void FixedUpdate()
    {
        
        Movimiento();
        Animations();
        //Para que haya friccion
        Vector3 fixedVelocity = rb.velocity;
        fixedVelocity.x *= 0.75f;

        
        if(grounded){
            rb.velocity = fixedVelocity;
        }
       

        //Limite Izquierda
        if(transform.position.x <= -80f){
            transform.position = new Vector3(-80f, transform.position.y, transform.position.z);
        }
    }

    void Update(){
        KunaiText.text = "X" + nKunais.ToString();
        currentTime += Time.deltaTime;
        Pintavidas();
       
       //Si las vida esta a 0 pues GG
       if(lifes <= 0){
        StartCoroutine(Destroy());
        }
       
       //Comprobar si esta en el suelo para saltar
        if (Input.GetButtonDown("Jump") && grounded){
           jump = true;
       }
      //Velocidad del parallax
       bg.SetVelocity(h);

        //Invertir Escala cuando va en negativo
        if(h > 0.1f){
            transform.localScale= new Vector3(0.52906f,0.52906f,1f);
            
        }else if(h < -0.1f){
            transform.localScale= new Vector3(-0.52906f,0.52906f,1f);

            
        }

    }


    public void Movimiento(){
        //Movimiento player
        h = Input.GetAxis("Horizontal");
        rb.AddForce(Vector2.right*h*movementSpeed*2);
        //Limitar velocidad al player
        SpeedMax = Mathf.Clamp(rb.velocity.x,-velocitymax,velocitymax);
        rb.velocity = new Vector2(SpeedMax,rb.velocity.y);
             
        
        //Salto
         //Si Jump es true salta
         if(jump){
           rb.AddForce(Vector2.up*FuerzaDeSalto,ForceMode2D.Impulse);
           jump = false;
       }
    }

    public void OnCollisionStay2D(Collision2D other) {
        if(other.gameObject.tag == "Suelo"){
            grounded = true;     
        }
        if(other.gameObject.tag == "Pinchos"){
            lifes = 0;
            StartCoroutine(Destroy());
        }
    }
     public void OnCollisionExit2D(Collision2D other) {
        if(other.gameObject.tag == "Suelo"){
            grounded = false;
        }
    } 
    public void OnCollisionEnter2D(Collision2D other){
        if(other.gameObject.tag == "KunaiCofre"){
            nKunais = 5;
        }
    }
   
    //ANIMACIONES
    public void Animations(){
        anim.SetBool("Grounded", grounded);
       //Hacer animacion de atacar
       if (Input.GetKeyDown(KeyCode.C) && grounded){
           //Ejecutar animacion de atacar y sonido
            Swort.Play();
            anim.SetTrigger("Attack");
        }
        else if(Input.GetKeyDown(KeyCode.C) && !grounded){
            //Ejecutar animacion de atacar mientras estas saltando
            Swort.Play();
            anim.SetTrigger("jump_attack");
        }
        //Tirar Kunai
        if(nKunais > 0){
            if (Input.GetKeyDown(KeyCode.X) && grounded){
                if(currentTime >= timeToThrow){
                    //Ejecutar animacion de atacar
                    currentTime = 0;
                    anim.SetTrigger("attack_throw");
                    kunai.Kunaii();
                    nKunais --;
                    KunaiSound.Play();
                    Debug.Log(nKunais);
                }
            }
            //Ejecutar animacion de atacar mientras estas saltando
            else if(Input.GetKeyDown(KeyCode.X) && !grounded){
                if(currentTime >= timeToThrow){
                    currentTime = 0;
                    anim.SetTrigger("jump_throw");
                    kunai.Kunaii();
                    nKunais --;
                    Debug.Log(nKunais);
                }
            }  
        } 
       //Si la velocidad es mayor que 0.1f(Valor indicado en el animation) ejecuta la animacion de
       anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
    }
    
    //Sistema de Vidas
        private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Swart"){
            lifes--;
            StartCoroutine(ColorRed());
            //anim.SetTrigger("Hurt");
            }
        else if(other.tag =="Key"){
            haveKey = true;
            uiKey.SetActive(true);
            Debug.Log(haveKey);
            Destroy(other.gameObject);
        }
        else if(other.tag == "Platano"){
            if(lifes < 5){
                lifes ++;
            }
        }
    }

        IEnumerator Destroy(){
        anim.SetTrigger("Dead");
        yield return new WaitForSeconds(0.8f);
        SceneManager.LoadScene("GameOverScreen");
         gameObject.SetActive(false);
         
        }
        IEnumerator ColorRed(){
            m_SpriteRenderer.color = new Color(255f, 0f, 0f, 255f); 
            yield return new WaitForSeconds(0.3f);
            m_SpriteRenderer.color = new Color(255f, 255f, 255f, 255f); 
            yield return new WaitForSeconds(0.3f);
            m_SpriteRenderer.color = new Color(255f, 0f, 0f, 255f); 
            yield return new WaitForSeconds(0.3f);
            m_SpriteRenderer.color = new Color(255f, 255f, 255f, 255f); 

        }

        public void Pintavidas(){
            if(lifes == 5){
                uiLifes[0].SetActive(true);
                uiLifes[1].SetActive(true);
                uiLifes[2].SetActive(true);
                uiLifes[3].SetActive(true);
                uiLifes[4].SetActive(true);
            }
            else if(lifes == 4){
                uiLifes[0].SetActive(true);
                uiLifes[1].SetActive(true);
                uiLifes[2].SetActive(true);
                uiLifes[3].SetActive(true);
                uiLifes[4].SetActive(false);
            }
            else if(lifes == 3){
                uiLifes[0].SetActive(true);
                uiLifes[1].SetActive(true);
                uiLifes[2].SetActive(true);
                uiLifes[3].SetActive(false);
                uiLifes[4].SetActive(false);
            }
            else if(lifes == 2){
                uiLifes[0].SetActive(true);
                uiLifes[1].SetActive(true);
                uiLifes[2].SetActive(false);
                uiLifes[3].SetActive(false);
                uiLifes[4].SetActive(false);
            }
            else if(lifes == 1){
                uiLifes[0].SetActive(true);
                uiLifes[1].SetActive(false);
                uiLifes[2].SetActive(false);
                uiLifes[3].SetActive(false);
                uiLifes[4].SetActive(false);
            }
            else if(lifes == 0){
                uiLifes[0].SetActive(false);
                uiLifes[1].SetActive(false);
                uiLifes[2].SetActive(false);
                uiLifes[3].SetActive(false);
                uiLifes[4].SetActive(false);
            }
        }
}
