﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KunaiManager : MonoBehaviour
{
    public GameObject arma;
    public PlayerController player;

    public void Kunaii(){
        StartCoroutine(InstanciaKunai());
    }
    IEnumerator InstanciaKunai(){
        if(player.h > 0.1f || player.transform.localScale.x > 0){
            yield return new WaitForSeconds(0.3f);
            Instantiate(arma, transform.position, Quaternion.identity, null);
        }
        else if(player.h < 0.1f || player.transform.localScale.x < 0){
            yield return new WaitForSeconds(0.3f);
            Instantiate(arma, transform.position, Quaternion.Euler(0,0,-180), null);
        }
    }
}
