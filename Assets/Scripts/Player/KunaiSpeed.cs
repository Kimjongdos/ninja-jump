﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KunaiSpeed : MonoBehaviour
{
    public int speed =30;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.right*speed*Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.gameObject.tag == "Enemy" ){
            Destroy(gameObject);
        }
        else if(other.gameObject.tag == "Suelo"){
            StartCoroutine(Clavado());
        }
    }
    IEnumerator Clavado(){
        speed = 0;
        yield return new WaitForSeconds(10f);
        Destroy(gameObject);

    }
}
