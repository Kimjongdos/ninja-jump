﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ninja_Attack : MonoBehaviour
{
    private Collider2D AttackCollider;
     void Awake()
    {
        AttackCollider = GetComponent<Collider2D>();
        AttackCollider.enabled = false;

    }
    void Update(){        
        StartCoroutine(ColliderAttack());
    }
    public void OnTriggerEnter2D(Collider2D other){
        StartCoroutine(ColliderAttack());
    }
    IEnumerator ColliderAttack(){
        if (Input.GetKeyDown(KeyCode.C)){
        AttackCollider.enabled = true;
        Debug.Log("Attack True");
        //Cadencia Collider
        yield return new WaitForSeconds(0.4f);
        Debug.Log("Attack False");
        AttackCollider.enabled = false;
        }
    }
}
