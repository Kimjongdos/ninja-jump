﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pendulum2 : MonoBehaviour
{
    public float speed;
    private HingeJoint2D hingeJoint2D;
    private JointMotor2D jointMotor2D;
    private float currentTime = 0;
    [SerializeField] float time;
    void Start(){
        hingeJoint2D = GetComponent<HingeJoint2D>();
        jointMotor2D = hingeJoint2D.motor;
    }

    void Update(){
        currentTime += Time.deltaTime;
        JointMotor2D motor = hingeJoint2D.motor;
        motor.motorSpeed = speed;
        hingeJoint2D.motor = motor;

        if(currentTime >= time){
            currentTime = -time;
            speed = speed * -1;
        }
    }
}
