﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlsManager : MonoBehaviour
{
    void Update(){
        if(Input.GetKey(KeyCode.Escape)){
            SceneManager.LoadScene("MenuScreen");
        }
    }
    public void ReturnToMenu(){
        SceneManager.LoadScene("MenuScreen");
    }

}
