﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
 
public class PauseManager : MonoBehaviour
{
    private bool isPaused = false;
    [SerializeField] GameObject PauseCanvas;
   
  
    public void Continue(){
        Debug.Log("he pulsado");
        Cursor.visible = false;
         Time.timeScale = 1.0f;
         PauseCanvas.SetActive(false);
        isPaused = false;
    }
 
    public void Quit(){
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("MenuScreen");
    }
 
 

    void Update()
    {
        if (!isPaused && Input.GetKeyDown(KeyCode.Escape))
        {
            ActivatePause();
        }
    }
 
    void ActivatePause(){
        Cursor.visible = true;
        isPaused = true;
        
        PauseCanvas.SetActive(true);
      
       
 
        Time.timeScale = 0;
    }
    

}