﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pergamino : MonoBehaviour
{
    public float SecondsToDestroy;

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(Pergaminoo());
    }
    IEnumerator Pergaminoo(){
        yield return new WaitForSeconds(SecondsToDestroy);
        Destroy(gameObject);
    }
}
