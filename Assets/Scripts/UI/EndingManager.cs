﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndingManager : MonoBehaviour
{
    public Text score;
    public Text record;
    public GameObject[] cartel;

    void Awake(){
        score.text =  PlayerPrefs.GetFloat("ActualMin").ToString("f0") + ":" + PlayerPrefs.GetFloat("ActualSec").ToString("f0");
        record.text = PlayerPrefs.GetFloat("RecordMin").ToString("f0") + ":" + PlayerPrefs.GetFloat("RecordSec").ToString("f0");
        if(PlayerPrefs.GetInt("UWin") == 0){
            cartel[1].SetActive(true);
            cartel[0].SetActive(false);
        }
        else{
            cartel[1].SetActive(false);
            cartel[0].SetActive(true);
        }
    }
    void Start(){
        Cursor.visible = true;
    }

    public void Restart(){
        SceneManager.LoadScene("GameplayScreen");
    }

    public void Menu(){
        SceneManager.LoadScene("MenuScreen");
    }
    public void Exit(){
        Application.Quit();
    }
}
