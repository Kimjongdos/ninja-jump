﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
public AudioSource click;
    
    void Start(){
        Cursor.visible = true;
    }
    public void PulsaStart(){
            click.Play(); 
            SceneManager.LoadScene("GamePlayScreen");
    }
        public void PulsaCredits(){
            click.Play(); 
            SceneManager.LoadScene("CreditsMenu");
    }
     public void PulsaControls(){
            click.Play(); 
            SceneManager.LoadScene("ControlsScreen");
    } 
    public void PulsaCreditos(){
        click.Play();
        SceneManager.LoadScene("CreditsMenu");
 
    }
    public void PulsaExit(){
        Application.Quit();
    }

}
