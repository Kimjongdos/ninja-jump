﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCounter : MonoBehaviour
{
    public Text timerText;
    private float startTime;
    private float t;
    public PlayerController player;
    private string minutes;
    private string seconds;

    void Start(){
        startTime = Time.time;
    }

    void Update(){
        if(player.lifes > 0){
             t = Time.time - startTime;

             minutes = ((int) t / 60).ToString();
             seconds = (t % 60). ToString("f0");
             timerText.text = minutes + ":" + seconds;
        }
        else{
            if(t/60 > GetRecordMin()){
                SaveMin((int)t/60);
                SaveSec(t % 60);
            }
            SaveActualMin(t/60);
            SaveActualSec(t % 60);
        }
    }
    //PlayerPrefs

    //MINUTES
    public float GetRecordMin(){
        return PlayerPrefs.GetFloat("RecordMin", 0);
    }
    public void SaveMin(float minutes){
        PlayerPrefs.SetFloat("RecordMin", minutes);
    }

    public float GetActualMin(){
        return PlayerPrefs.GetFloat ("ActualMin", 0);
    }
    public void SaveActualMin(float minutes){
        PlayerPrefs.SetFloat("ActualMin", minutes);
    }

    //SECONDS
    public float GetSecondsMin(){
        return PlayerPrefs.GetFloat("RecordSec", 0);
    }
    public void SaveSec(float seconds){
        PlayerPrefs.SetFloat("RecordSec", seconds);
    }
    public float GetActualSec(){
        return PlayerPrefs.GetFloat("ActualSec", 0);
    }
    public void SaveActualSec(float seconds){
        PlayerPrefs.SetFloat("ActualSec", seconds);
    }


}

