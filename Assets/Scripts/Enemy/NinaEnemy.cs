﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinaEnemy : MonoBehaviour
{
    public float visionRadius,attackRadius,speed;
    public GameObject player;
    GameObject swort;
    private Vector3 initialPosition;
    private Rigidbody2D rb2d;
    private Animator anim;
    public PlayerController ninja;

    public SpriteRenderer spriteRend;
    public BoxCollider2D boxCollider;
    private bool isFacingRight;
    //Ataque
    public float AttackSpeed = 2f;
    bool attacking;
    
    Vector3 target;
    //Lifes
    public int lifes = 3;
    
    void Start ()
    {
        initialPosition = transform.position;
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        swort = GameObject.Find("Swort");
        swort.SetActive(false);
        isFacingRight = true;
    }
    
    
    void Update ()
    { 
        if(lifes <= 0)
        {
            StartCoroutine(Destroy());
        }
        //El target es la posicion inicial del enemigo
        target = initialPosition;
        //Comprobar la posicion del enemigo hacia el jugador

                                         //Posicion Actual   //Posicion Player         //Posicion Actual    //Alcance     
        RaycastHit2D hit = Physics2D.Raycast(transform.position,player.transform.position - transform.position, visionRadius , 1 << LayerMask.NameToLayer("Default"));
    
        //Calcular la posicion del Raycast entre Enemigo y player(?)
        Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
        //Dibujar el RAY CAST de: Actual Posicion hacia JUGADOR de color ROJO
        Debug.DrawRay (transform.position, forward, Color.red);
    
        //SI el RayCast encuentra al Jugador,target es ahora la posicion del jugador
        if (hit.collider != null)
        {
            if (hit.collider.tag == "Player")
            {
                target = player.transform.position; //Player encontrado            
            }
        }
        //Creamos una variable FLOAT llamada DISTANCE
        //En DISTANCE calculamos la distancia del Jugador(TARGEt) hacia el jugador(transform.position) para saber cuanto hay que moverse
        float distance = Vector3.Distance(target, transform.position);
        //Creamos un Vector3 llamada DIR para saber la direccion
        //El normalized pasa el valor a -1,0,1. Esto haria que si el Enemigo esta a 17Y, pues seria 1, y si estuviera a -17Y seria -1
        Vector3 dir = (target - transform.position).normalized;

        if ((isFacingRight && dir.x < 0) || (!isFacingRight && dir.x > 0)) Flip();

        //Si el Player esta en el Rango de ataque el enemigo atacará
        if (target != initialPosition && distance < attackRadius) {
        //Ataca
        if(!attacking)StartCoroutine(Ataque());  //Si atacar es falso comienza la coruotina
    
        }
        else
        { //Si el enemigo NO esta en el rango de ataque el ENEMIGO se movera hacia el PLAYER
            rb2d.MovePosition(transform.position + dir *speed * Time.deltaTime);
            anim.SetBool("Movimiento",true);        
        }

        //Solucionar un bug que cuando el enemigo llege a su posicion otiginal no haga un lio de animaciones
        if (target == initialPosition && distance < 0.1f) {
            transform.position = initialPosition;
            //idle 
            anim.SetBool("Movimiento",false);
            Debug.DrawLine(transform.position, target, Color.green);
            }

    }
    void Flip()
    {
        isFacingRight = !isFacingRight;

        spriteRend.flipX = !isFacingRight;

        Vector2 offset = boxCollider.offset;
        offset.x *= -1;
        boxCollider.offset = offset;
    }

    //Dibujar los circulos de ataque y de vision hacia al Player
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
        Gizmos.DrawWireSphere(transform.position, attackRadius);

    }
        //Sistema de ataque
    IEnumerator Ataque()
    {
        attacking =true;
        if(target!= initialPosition)
        {
            Debug.Log("Atacando");
            anim.SetTrigger("Attack");
            swort.SetActive(true);
            yield return new WaitForSeconds(AttackSpeed);
        }
        swort.SetActive(false);
        attacking = false;
    }
        //Sistema de Vidas
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Espada")
        {
            lifes--;
            anim.SetTrigger("Hurt");
            Debug.Log("Impacto");
            
        }
        else if(other.tag == "Kunai")
        {
            lifes--;
            anim.SetTrigger("Hurt");
            Debug.Log("Impacto");
        }            
    }

    IEnumerator Destroy()
    {
        anim.SetTrigger("Life");
        yield return new WaitForSeconds(0.8f);
        gameObject.SetActive(false);
    }
}