﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Diamante : MonoBehaviour
{
    private CircleCollider2D cc2D;
    private SpriteRenderer sr;

    void Awake(){
        cc2D = GetComponent<CircleCollider2D>();
        sr = GetComponent<SpriteRenderer>();
    }
    void OnCollisionEnter2D(Collision2D other){
        if(other.gameObject.tag =="Player"){
            PlayerPrefs.SetInt("UWin", 1);
            StartCoroutine(GOWin());
        }
    }
    IEnumerator GOWin(){
        cc2D.enabled = false;
        sr.enabled = false;
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("GameOverScreen");
    }
}
