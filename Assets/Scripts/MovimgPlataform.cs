﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimgPlataform : MonoBehaviour
{
    public float speed;
    [SerializeField] float time;
    private float currentTime;

    void Update(){
        currentTime += Time.deltaTime;
        transform.Translate( new Vector3( speed * Time.deltaTime, 0, 0));
        if(currentTime >= time){
            currentTime = 0;
            speed = speed * -1;
        }
    }
}
