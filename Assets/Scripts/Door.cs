﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public PlayerController player;
    public GameObject door;
    private BoxCollider2D coll2D;

    void Awake(){
        coll2D = GetComponent<BoxCollider2D>();
    }

    void OnCollisionEnter2D(Collision2D other){
        if(other.gameObject.tag == "Player" && player.haveKey){
            coll2D.enabled = false;
            door.SetActive(true);
            
        }
    }

}


