﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rebote : MonoBehaviour
{
    public AudioSource rebote;

    // Update is called once per frame
    private void OnCollisionEnter2D(Collision2D other) {
        rebote.Play();
    }
}
